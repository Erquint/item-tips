# Glorfindel's Tip Pack

Extension pack to [ItemTips](https://enter-the-gungeon.thunderstore.io/package/Glorfindel/ItemTips) with tips for modded items. Supported mods:

  - [Expand The Gungeon](https://enter-the-gungeon.thunderstore.io/package/ApacheThunder/ExpandTheGungeon)
  - [Kyle's Custom Item Pack](https://enter-the-gungeon.thunderstore.io/package/KyleTheScientist/CustomItems)
  - [[Retrash's] Custom Items Collection](https://enter-the-gungeon.thunderstore.io/package/Retrash/Retrash_Custom_Items_Collection)

## Special Thanks
* [Dallan](https://enter-the-gungeon.thunderstore.io/package/Dallan/) for making the icon.

---

## Changelog

* 1.0.0 - Item tips for Expand the Gungeon, Kyle's Custom Items, and Retrash's Custom Items
* 1.0.1 - Fix descriptions for some Expand the Gungeon items: Portable Elevator, Portable Ship, Third Eye.