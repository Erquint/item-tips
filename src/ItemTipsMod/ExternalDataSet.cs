﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace ItemTipsMod
{
    public class ExternalDataSet
    {
        public ExternalMetadata Metadata;

        public Dictionary<string, ExternalPickupData> Items;

        public Dictionary<string, ExternalSynergyData> Synergies;
    }

    public class ExternalPickupData
    {
        public string Name;

        public string Notes;

        [JsonIgnore]
        public ExternalMetadata SourceMetadata;
    }

    public class ExternalSynergyData
    {
        public string Name;

        public string Notes;

        [JsonIgnore]
        public ExternalMetadata SourceMetadata;
    }

    public class ExternalMetadata
    {
        public string Name;

        public string Version;

        public string Url;
    }
}
