﻿using System;
using System.Collections.Generic;

namespace ItemTipsMod
{
    public class InternalDataSet
    {
        public ItemData[] Items = ArrayHelper<ItemData>.Empty;

        public SynergyData[] Synergies = ArrayHelper<SynergyData>.Empty;
    }

    public class SynergyData
    {
        public string Key;

        public PartsList OtherParts;

        // only used for cormorant when key is null.
        public string Name;

        // only used for cormorant when key is null.
        public string Effect;

        public SourceMetadata SourceMetadata;

        public bool InternalWouldGetSynergy()
        {
            if (OtherParts?.Parts == null)
            {
                return false;
            }

            bool wouldGetSynergy = false;
            foreach (var player in GameManager.Instance.AllPlayers)
            {
                int count = 0;
                foreach (var part in OtherParts.Parts)
                {
                    var pickupObject = PickupObjectDatabase.GetById(part);
                    if ((object)pickupObject != null)
                    {
                        if (player.HasPickupID(pickupObject.PickupObjectId))
                        {
                            count++;
                        }
                    }

                    if (OtherParts.PartOperand == PartOperand.One)
                    {
                        if (count > 0)
                        {
                            break;
                        }
                    }
                }

                if (OtherParts.PartOperand == PartOperand.One || OtherParts.PartOperand == PartOperand.OneOrMore)
                {
                    if (count > 0)
                    {
                        wouldGetSynergy = true;
                    }
                }
                else if (OtherParts.PartOperand == PartOperand.All)
                {
                    if (count == OtherParts.Parts.Length)
                    {
                        wouldGetSynergy = true;
                    }
                }
            }

            return wouldGetSynergy;
        }
    }

    public class PartsList
    {
        private PartOperand? _operand;

        public string Operand;

        public PartOperand PartOperand
        {
            get
            {
                if (!_operand.HasValue)
                {
                    if (string.IsNullOrEmpty(Operand))
                    {
                        _operand = PartOperand.Unknown;
                    }
                    else
                    {
                        if (string.Equals(Operand, "one+", StringComparison.OrdinalIgnoreCase))
                        {
                            _operand = PartOperand.OneOrMore;
                        }
                        else
                        {
                            try
                            {
                                _operand = (PartOperand)Enum.Parse(typeof(PartOperand), Operand, true);
                            }
                            catch (Exception)
                            {
                                _operand = PartOperand.Unknown;
                            }
                        }
                    }
                }

                return _operand.GetValueOrDefault();
            }
        }

        public int[] Parts;
    }

    public enum PartOperand
    {
        Unknown,
        One,
        OneOrMore,
        All
    }
}

