﻿namespace ItemTipsMod
{
    internal static class ArrayHelper<T>
    {
        public static T[] Empty { get; } = new T[0];
    }
}

