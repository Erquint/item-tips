﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ItemTipsMod
{
    public class PlayerControllerObserver : MonoBehaviour
    {
        private Func<PlayerController, IPlayerInteractable> getLastInteractable;

        public float TimeBetweenChecks = 1f;

        public Action EnteredCombat;

        public Action ExitedCombat;

        public Action<PlayerController, IPlayerInteractable> LastInteractableChanged;

        private float TimeCounter;

        private bool _currentlyInCombat = false;

        private IPlayerInteractable _lastInteractable;

        private void Start()
        {
            try
            {
                getLastInteractable = ReflectionHelper.CreatePrivateFieldGetter<PlayerController, IPlayerInteractable>("m_lastInteractionTarget");
            }
            catch (Exception e)
            {
                StaticLogger.LogInfo($"Cannot inspect player interactions: {e}");
            }
        }

        private void Update()
        {
            TimeCounter -= Time.deltaTime;
            if (TimeCounter <= 0)
            {
                TimeCounter = TimeBetweenChecks;
                try
                {
                    // check combat
                    bool inCombat = false;
                    foreach (var player in GameManager.Instance.AllPlayers)
                    {
                        if (player.IsInCombat)
                        {
                            inCombat = true;
                            break;
                        }

                        var currentLastInteractable = getLastInteractable(player);
                        if (_lastInteractable != currentLastInteractable)
                        {
                            _lastInteractable = currentLastInteractable;
                            LastInteractableChanged?.Invoke(player, _lastInteractable);
                            break;
                        }
                    }

                    if (_currentlyInCombat != inCombat)
                    {
                        _currentlyInCombat = inCombat;
                        if (inCombat)
                        {
                            EnteredCombat?.Invoke();
                        }
                        else
                        {
                            ExitedCombat?.Invoke();
                        }
                    }
                }
                catch (Exception e)
                {
                    StaticLogger.LogDebug($"Error in Update: {e}");
                }
            }
        }
    }
}

