﻿using UnityEngine;

namespace ItemTipsMod
{
    internal struct TextInfo
    {
        public TextInfo(string text, Vector2 size)
        {
            LabelText = text;
            LabelSize = size;
        }


        /// <summary>
        /// Text that will appear in the tool tip
        /// </summary>
        public readonly string LabelText;

        public readonly Vector2 LabelSize;
    }
}

