﻿using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ItemTipsMod
{
    internal static class JsonHelper
    {
        private static readonly JsonSerializerSettings DefaultSettings = new JsonSerializerSettings()
        {
            Formatting = Formatting.Indented,
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        private static readonly JsonSerializer Serializer = JsonSerializer.Create(DefaultSettings);

        public static T DeserializeFromFile<T>(string filePath)
        {
            using (var stream = new StreamReader(filePath))
            using (var reader = new JsonTextReader(stream))
            {
                return Serializer.Deserialize<T>(reader);
            }
        }

        public static T DeserializeFromStream<T>(Stream inputStream)
        {
            using (var stream = new StreamReader(inputStream))
            using (var reader = new JsonTextReader(stream))
            {
                return Serializer.Deserialize<T>(reader);
            }
        }

        public static void SerializeToFile(string filePath, object obj)
        {
            using (var stream = new StreamWriter(filePath, false))
            using (var writer = new JsonTextWriter(stream))
            {
                Serializer.Serialize(writer, obj);
            }
        }
    }
}

