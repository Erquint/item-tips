﻿namespace ItemTipsMod
{
    public enum TipSource
    {
        Gun,
        ActiveItem,
        PassiveItem,
        ShopItem,
        RewardPedestal,
        ChestPredicted
    }
}

