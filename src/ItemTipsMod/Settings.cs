﻿using System;
using UnityEngine;

namespace ItemTipsMod
{
    public class Settings
    {
        public int LineWidth = 40;

        /// <summary>
        ///  as a percentage of full width
        /// </summary>
        public float Left = 0.01f;

        /// <summary>
        /// As a percentage of full height
        /// </summary>
        public float Top = 0.20f;

        public Vector2 GetSize(int linesHeight)
        {
            return new Vector2(11f * LineWidth, 32f * linesHeight);
        }
    }
}

