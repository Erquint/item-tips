﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemTipsMod
{
    internal static class TipBuilder
    {
        public static void AppendQualityDescriptor(PickupObject pickup, Action<string> appendLine)
        {
            // heart container items span quality tiers
            if (pickup.quality == PickupObject.ItemQuality.COMMON ||
                pickup.quality == PickupObject.ItemQuality.EXCLUDED ||
                pickup.quality == PickupObject.ItemQuality.SPECIAL)
            {
                return;
            }

            if (pickup.ItemSpansBaseQualityTiers || pickup.ItemRespectsHeartMagnificence)
            {
                appendLine($"Quality: C/B/A");
            }
            else
            {
                // todo colors $"<color=#FF0000>({data.Quality[0]}) {data.Name}</color>"
                //var quality = string.Join("/", item.Quality);
                appendLine($"Quality: {pickup.quality}");
            }
        }

        public static void AppendActiveCooldownType(PickupObject pickup, Action<string> appendLine)
        {
            if (pickup is PlayerItem active)
            {
                if (active.roomCooldown > 0)
                {
                    appendLine("Cooldown: Room");
                }
                else if (active.damageCooldown > 0)
                {
                    appendLine("Cooldown: Damage");
                }
                else if (active.timeCooldown > 0)
                {
                    appendLine("Cooldown: Time");
                }
            }
        }

        public static void AppendModifiers(PickupObject pickup, Action<PlayerStats.StatType, string> appendLine)
        {
            var statModifiers = new List<StatModifier>();
            switch (pickup)
            {

                case Gun g:
                    if (g.passiveStatModifiers != null)
                    {
                        statModifiers.AddRange(g.passiveStatModifiers);
                    }
                    break;
                case BasicStatPickup statPickup:
                    if (statPickup.passiveStatModifiers != null)
                    {
                        statModifiers.AddRange(statPickup.passiveStatModifiers);
                    }

                    if (statPickup.modifiers != null)
                    {
                        statModifiers.AddRange(statPickup.modifiers);
                    }
                    break;
                case PassiveItem passive:
                    if (passive.passiveStatModifiers != null)
                    {
                        statModifiers.AddRange(passive.passiveStatModifiers);
                    }
                    break;
                case PlayerItem active:
                    if (active.passiveStatModifiers != null)
                    {
                        statModifiers.AddRange(active.passiveStatModifiers);
                    }
                    break;
                default:
                    StaticLogger.LogDebug($"Could not get stat modifiers: {pickup.EncounterNameOrDisplayName}");
                    return;
            }

            if (statModifiers.Count > 0)
            {
                float curse = 0f;
                float coolness = 0f;
                foreach (var modifier in statModifiers)
                {
                    if (modifier.statToBoost == PlayerStats.StatType.Curse)
                    {
                        curse += modifier.amount;
                    }
                    else if (modifier.statToBoost == PlayerStats.StatType.Coolness)
                    {
                        coolness += modifier.amount;
                    }
                }

                if (curse > 0)
                {
                    appendLine(PlayerStats.StatType.Curse, $"Curse: +{curse}");
                }
                if (coolness > 0)
                {
                    appendLine(PlayerStats.StatType.Coolness, $"Coolness: +{coolness}");
                }
            }
        }
    }
}
