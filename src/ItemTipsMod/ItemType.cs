﻿namespace ItemTipsMod
{
    public enum ItemType
    {
        None,
        Gun,
        Passive,
        Active,
        Synergy
    }
}

