﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Gungeon;

namespace ItemTipsMod
{
    internal static class DebugHelper
    {
        public static void AddDebugCommands(ItemTipsModule module)
        {
#if DEBUG
            ETGModConsole.Commands.AddUnit("dumpitems", args =>
            {
                if (args.Length == 0)
                {
                    ETGModConsole.Log("No valid quality levels to filter on");
                    return;
                }

                var qualities = new HashSet<PickupObject.ItemQuality>();
                foreach (string arg in args)
                {
                    try
                    {
                        var quality = (PickupObject.ItemQuality)Enum.Parse(typeof(PickupObject.ItemQuality), arg, true);
                        qualities.Add(quality);
                    }
                    catch (Exception)
                    {
                        ETGModConsole.Log($"Could not parse argument '{arg}'");
                    }
                }

                if (qualities.Count == 0)
                {
                    ETGModConsole.Log("No valid quality levels to filter on");
                    return;
                }

                if (PickupObjectDatabase.Instance == null)
                {
                    ETGModConsole.Log("PickupObjectDatabase is null");
                    return;
                }

                if (PickupObjectDatabase.Instance.Objects == null)
                {
                    ETGModConsole.Log("PickupObjectDatabase.Objects is null");
                    return;
                }

                foreach (var obj in PickupObjectDatabase.Instance.Objects)
                {
                    if ((object)obj == null)
                    {
                        continue;
                    }

                    if (qualities.Contains(obj.quality))
                    {
                        ETGModConsole.Log($"{obj.name}: {obj.EncounterNameOrDisplayName}");
                    }
                }
            });

            ETGModConsole.Commands.AddUnit("findsynergy", args =>
            {
                if (args.Length < 1)
                {
                    ETGModConsole.Log("Requires 1 item id as a parameter");
                    return;
                }

                string id = args[0];
                if (!Game.Items.ContainsID(id))
                {
                    ETGModConsole.Log(string.Format("Invalid item ID {0}!", id), false);
                    return;
                }

                var pickup = Game.Items.Get(id);
                ETGModConsole.Log($"{pickup.EncounterNameOrDisplayName}");
                var pickupId = Game.Items.Get(id).PickupObjectId;
                foreach (var synergy in GameManager.Instance.SynergyManager.synergies)
                {
                    bool contained = false;
                    if (synergy.MandatoryGunIDs.Contains(pickupId))
                    {
                        contained = true;
                    }
                    else if (synergy.MandatoryItemIDs.Contains(pickupId))
                    {
                        contained = true;
                    }
                    else if (synergy.OptionalGunIDs.Contains(pickupId))
                    {
                        contained = true;
                    }
                    else if (synergy.OptionalItemIDs.Contains(pickupId))
                    {
                        contained = true;
                    }

                    if (contained)
                    {
                        ETGModConsole.Log($"NameKey: {synergy.NameKey}");
                        ETGModConsole.Log($"String: {StringTableManager.GetSynergyString(synergy.NameKey)}");
                    }
                }
            });

            ETGModConsole.Commands.AddUnit("debugitemdata", args =>
            {
                if (args.Length < 1)
                {
                    ETGModConsole.Log("Requires 1 item id as a parameter");
                    return;
                }

                string id = args[0];
                if (!Game.Items.ContainsID(id))
                {
                    ETGModConsole.Log(string.Format("Invalid item ID {0}!", id), false);
                    return;
                }

                var item = Game.Items.Get(id);
                var cache = module.GetDataCache();
                if (cache.Pickups.TryGetValue(item.PickupObjectId, out var itemData))
                {
                    ETGModConsole.Log($"Name: {itemData.Name}");
                    ETGModConsole.Log($"Notes: {itemData.Notes}");
                    if (itemData.SourceMetadata == SourceMetadata.InternalSource)
                    {
                        ETGModConsole.Log($"Source: internal");
                    }
                    else if (itemData.SourceMetadata == null)
                    {
                        ETGModConsole.Log($"Source: unknown");
                    }
                    else
                    {
                        ETGModConsole.Log($"Source: {itemData.SourceMetadata.Name} {itemData.SourceMetadata.Version}");
                    }

                    var synergyCache = module.GetSynergyCache();
                    if (synergyCache.ItemIndex.TryGetValue(item.PickupObjectId, out var synergyGroups))
                    {
                        ETGModConsole.Log($"Synergies:");
                        foreach (var group in synergyGroups)
                        {
                            ETGModConsole.Log($"{group.NameKey}");
                        }
                    }

                }
                else
                {
                    ETGModConsole.Log("No data for item");
                }
            });

            ETGModConsole.Commands.AddUnit("debugsynergydata", args =>
            {
                if (args.Length < 1)
                {
                    ETGModConsole.Log("Requires 1 synergy key as a parameter");
                    return;
                }

                string key = args[0];
                var cache = module.GetDataCache();
                if (cache.Synergies.TryGetValue(key, out var synergyData))
                {
                    ETGModConsole.Log($"Name: {synergyData.Name}");
                    ETGModConsole.Log($"Effect: {synergyData.Effect}");
                    if (synergyData.SourceMetadata == SourceMetadata.InternalSource)
                    {
                        ETGModConsole.Log($"Source: internal");
                    }
                    else if (synergyData.SourceMetadata == null)
                    {
                        ETGModConsole.Log($"Source: unknown");
                    }
                    else
                    {
                        ETGModConsole.Log($"Source: {synergyData.SourceMetadata.Name} {synergyData.SourceMetadata.Version}");
                    }
                }
                else
                {
                    ETGModConsole.Log("No data for synergy");
                }
            });

            // spew drops an item on the ground instead of directly giving it to the player.
            ETGModConsole.Commands.AddUnit("spew", args =>
            {
                if (!ETGModConsole.ArgCount(args, 1, 2))
                {
                    return;
                }
                if (!GameManager.Instance.PrimaryPlayer)
                {
                    ETGModConsole.Log("Couldn't access Player Controller", false);
                    return;
                }
                string id = args[0];
                if (!Game.Items.ContainsID(id))
                {
                    ETGModConsole.Log(string.Format("Invalid item ID {0}!", id), false);
                    return;
                }
                var item = Game.Items.Get(id);
                ETGModConsole.Log($"Attempting to spawn item ID {args[0]} (numeric {id}), class {item.GetType()})");

                if (!GameManager.Instance.PrimaryPlayer.IsPlaying())
                {
                    throw new Exception("Tried to give item to inactive player controller");
                }

                LootEngine.SpewLoot(Game.Items[id].gameObject, GameManager.Instance.PrimaryPlayer.specRigidbody.UnitCenter);
            });

            ETGModConsole.Commands.AddUnit("cooldown", args =>
            {
                try
                {
                    if (args.Length == 0)
                    {
                        CooldownLabelBehavior.Instance.labelCollection.ForEach(c =>
                        {
                            ETGModConsole.Log($"Settings:\nx:{c.BaseRelativeX}\ny:{c.BaseRelativeY}\nanchor:{c.Anchor}");
                        });
                    }
                    else if (args.Length >= 2)
                    {
                        string parameter = args[0];
                        string stringVal = args[1];
                        int playerIndex = 0;
                        if (args.Length > 2)
                        {
                            if (!int.TryParse(args[2], out playerIndex))
                            {
                                playerIndex = 0;
                            }
                        }

                        if (parameter == "relativex")
                        {
                            if (int.TryParse(stringVal, out int val))
                            {
                                CooldownLabelBehavior.Instance.labelCollection[playerIndex].BaseRelativeX = val;
                                ETGModConsole.Log($"Set [{playerIndex}].RelativeX = {val}");
                            }
                        }
                        else if (parameter == "relativey")
                        {
                            if (int.TryParse(stringVal, out int val))
                            {
                                CooldownLabelBehavior.Instance.labelCollection[playerIndex].BaseRelativeY = val;
                                ETGModConsole.Log($"Set [{playerIndex}].RelativeY = {val}");
                            }
                        }
                        else if (parameter == "anchor")
                        {
                            try
                            {
                                var val = (dfAnchorStyle)Enum.Parse(typeof(dfAnchorStyle), stringVal, true);
                                CooldownLabelBehavior.Instance.labelCollection[playerIndex].Anchor = val;
                                ETGModConsole.Log($"Set [{playerIndex}].Anchor = {val}");
                            }
                            catch { }
                        }
                        else if (parameter == "visible")
                        {
                            if (bool.TryParse(stringVal, out var val))
                            {
                                CooldownLabelBehavior.Instance.SetVisible(val);
                                ETGModConsole.Log($"Set visible = {val}");
                            }
                        }

                        CooldownLabelBehavior.Instance.UpdateLabelPostion();
                    }
                    else
                    {
                        ETGModConsole.Log("requires two parameters");
                    }
                }
                catch (Exception e)
                {
                    ETGModConsole.Log($"Error {e}");
                }
            });
#endif
        }

        public static ElapsedHelper LogElapsed(string label)
        {
            return new ElapsedHelper(label, Stopwatch.GetTimestamp());
        }

        internal struct ElapsedHelper : IDisposable
        {
            public ElapsedHelper(string label, long startTimestamp)
            {
                Label = label;
                StartTimestamp = startTimestamp;
            }

            public readonly string Label;

            public readonly long StartTimestamp;

            public void Dispose()
            {
                long endTimestamp = Stopwatch.GetTimestamp();
                float elapsed = endTimestamp - StartTimestamp;

                if (elapsed <= 0)
                {
                    StaticLogger.LogDebug($"{Label} Elapsed: 0 ms");
                    return;
                }

                float sec = elapsed / Stopwatch.Frequency;
                float ms = sec * 1000f;
                StaticLogger.LogDebug($"{Label} Elapsed: {ms} ms");
            }
        }
    }
}
