﻿using System.Collections.Generic;

namespace ItemTipsMod
{
    public class AdvancedSynergyCache
    {
        public AdvancedSynergyCache()
            : this(ArrayHelper<AdvancedSynergyEntry>.Empty)
        {
        }

        public AdvancedSynergyCache(AdvancedSynergyEntry[] sourceEntries)
        {
            SourceEntries = sourceEntries;
            Build();
        }

        public AdvancedSynergyEntry[] SourceEntries;

        public Dictionary<string, SynergyGroup> SynergyKeyIndex;
        public Dictionary<int, List<SynergyGroup>> ItemIndex;

        private void Build()
        {
            SynergyKeyIndex = new Dictionary<string, SynergyGroup>();
            ItemIndex = new Dictionary<int, List<SynergyGroup>>();

            foreach (var synergy in SourceEntries)
            {
                if (string.IsNullOrEmpty(synergy.NameKey) ||
                    synergy.ActivationStatus == SynergyEntry.SynergyActivation.DEMO ||
                    synergy.ActivationStatus == SynergyEntry.SynergyActivation.INACTIVE)
                {
                    continue;
                }

                if (SynergyKeyIndex.TryGetValue(synergy.NameKey, out var group))
                {
                    group.Entries.Add(synergy);
                }
                else
                {
                    SynergyKeyIndex[synergy.NameKey] = new SynergyGroup(synergy.NameKey)
                    {
                        Entries =
                        {
                            synergy
                        }
                    };
                }

            }

            foreach (var kvp in SynergyKeyIndex)
            {
                var nameKey = kvp.Key;
                var group = kvp.Value;
                foreach (var synergy in group.Entries)
                {
                    AddSynergyEntries(synergy.MandatoryGunIDs, group);
                    AddSynergyEntries(synergy.MandatoryItemIDs, group);
                    AddSynergyEntries(synergy.OptionalGunIDs, group);
                    AddSynergyEntries(synergy.OptionalItemIDs, group);
                }
            }
        }

        private void AddSynergyEntries(List<int> ids, SynergyGroup group)
        {
            if (ids != null)
            {
                foreach (var id in ids)
                {
                    if (ItemIndex.TryGetValue(id, out var list))
                    {
                        if (!list.Contains(group))
                        {
                            list.Add(group);
                        }
                    }
                    else
                    {
                        ItemIndex[id] = new List<SynergyGroup>() { group };
                    }
                }
            }
        }
    }

    /// <summary>
    /// A group of advanced synergy entries with the same name key. This isn't strictly necessary 
    /// for the base game, but custom synergies are being added that are duplicates.
    /// (He who shall remain nameless has done this in his item mod pack!)
    /// </summary>
    public class SynergyGroup
    {
        public SynergyGroup(string nameKey)
        {
            NameKey = nameKey;
        }

        public readonly string NameKey;

        public List<AdvancedSynergyEntry> Entries = new List<AdvancedSynergyEntry>();

        public bool SynergyIsAvailable(PlayerController p1, PlayerController p2, int additionalID = -1)
        {
            foreach (var entry in Entries)
            {
                if (entry.SynergyIsAvailable(p1, p2, additionalID))
                {
                    return true;
                }
            }

            return false;
        }
    }
}

