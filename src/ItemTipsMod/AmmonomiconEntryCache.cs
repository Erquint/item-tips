﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItemTipsMod
{
    public sealed class AmmonomiconEntryCache
    {
        private readonly PickupAndSynergyDataCache _dataCache;
        private readonly AdvancedSynergyCache _synergyCache;
        private StringTableManager.GungeonSupportedLanguages _currentLanguage;
        private Dictionary<int, string> _entryCache;

        public AmmonomiconEntryCache(AdvancedSynergyCache synergyCache, PickupAndSynergyDataCache dataCache)
        {
            _synergyCache = synergyCache;
            _dataCache = dataCache;
            _entryCache = new Dictionary<int, string>();
        }

        public StringTableManager.GungeonSupportedLanguages GetCacheLanguage()
        {
            return _currentLanguage;
        }

        public void SetCacheLanguage(StringTableManager.GungeonSupportedLanguages value)
        {
            if (value != _currentLanguage)
            {
                _entryCache = new Dictionary<int, string>();
                _currentLanguage = value;
            }
        }

        public string GetEntry(int pickupId, string origEntry)
        {
            if (pickupId < 0)
            {
                return origEntry;
            }

            if (_entryCache.TryGetValue(pickupId, out string value))
            {
                return value;
            }
            else
            {
                if (!_dataCache.Pickups.TryGetValue(pickupId, out var data))
                {
                    data = null;
                }

                var pickup = PickupObjectDatabase.GetById(pickupId);
                string extended = GenerateAmmonomiconText(pickup, data);
                var updated = $"{origEntry}\n{extended}".Trim();
                _entryCache[pickupId] = updated;
                return updated;
            }
        }

        private string GenerateAmmonomiconText(PickupObject pickup, ItemData item)
        {
            var builder = new StringBuilder();
            builder.AppendLine("----------");

            TipBuilder.AppendQualityDescriptor(pickup, line => builder.AppendLine(line));
            TipBuilder.AppendActiveCooldownType(pickup, line => builder.AppendLine(line));

            if (item != null)
            {
                builder.AppendLine(item.Notes);
            }

            TipBuilder.AppendModifiers(pickup, (stat, line) => builder.AppendLine(line));

            // using visible names bc sometimes there are two different keys with the same display name
            // e.g. Smart Bombs is actually two entries. One for bomb, the other for homing bombs.
            var handledSynergies = new HashSet<string>();
            if (_synergyCache.ItemIndex.TryGetValue(pickup.PickupObjectId, out var synergyList))
            {
                if (synergyList.Count > 0)
                {
                    builder.AppendLine("Synergies:");

                    // 'real' synergies
                    foreach (var synergy in synergyList)
                    {
                        string synergyName = StringTableManager.GetSynergyString(synergy.NameKey);

                        if (!handledSynergies.Add(synergyName))
                            continue;

                        bool isAvailable = synergy.SynergyIsAvailable(GameManager.Instance.PrimaryPlayer, GameManager.Instance.SecondaryPlayer);
                        string indicator = isAvailable ? "*" : "";
                        if (_dataCache.Synergies.TryGetValue(synergy.NameKey, out var synergyData))
                        {
                            builder.AppendLine($" - {indicator}{synergyName}{indicator}: {synergyData.Effect}");
                        }
                        else
                        {
                            builder.AppendLine($" - {indicator}{synergyName}{indicator}");
                        }
                    }
                }
            }

            if (item != null)
            {
                if (item.Synergies.Length > 0)
                {
                    if (handledSynergies.Count == 0)
                    {
                        builder.AppendLine("Synergies:");
                    }

                    // psuedo synergies like Cormorant
                    foreach (var synergyData in item.Synergies)
                    {
                        string synergyName = synergyData.GetLocalizedSynergyName();

                        // prevent duplicate synergies details
                        if (!handledSynergies.Add(synergyName))
                            continue;

                        string effect = synergyData.Effect;
                        bool isAvailable = synergyData.InternalWouldGetSynergy();
                        string indicator = isAvailable ? "*" : "";
                        if (!string.IsNullOrEmpty(effect))
                        {
                            builder.AppendLine($" - {indicator}{synergyName}{indicator}: {effect}");
                        }
                        else
                        {
                            builder.AppendLine($" - {indicator}{synergyName}{indicator}");
                        }
                    }
                }
            }

            return builder.ToString();
        }
    }
}
