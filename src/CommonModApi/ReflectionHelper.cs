﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CommonModApi
{
    internal delegate void Action<T1, T2, T3, T4, T5>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);

    internal delegate void Action<T1, T2, T3, T4, T5, T6>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6);

    internal delegate void Action<T1, T2, T3, T4, T5, T6, T7>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7);

    internal static class ReflectionHelper
    {
        private static Type[] ActionDelegateTypes = {
            typeof(Action),
            typeof(Action<>),
            typeof(Action<,>),
            typeof(Action<,,>),
            typeof(Action<,,,>),
            typeof(Action<,,,,>), // our delegate type
            typeof(Action<,,,,,>),
            typeof(Action<,,,,,,>),
        };

        private static Type[] FuncDelegateTypes = {
            typeof(Func<>),
            typeof(Func<,>),
            typeof(Func<,,>),
            typeof(Func<,,,>),
            typeof(Func<,,,,>),
        };

        public static Func<TTarget, TField> CreatePrivateFieldGetter<TTarget, TField>(string fieldName)
        {
            var field = typeof(TTarget).GetField(fieldName, BindingFlags.Instance | BindingFlags.NonPublic);
            if (field == null)
            {
                throw new ArgumentException($"Private instance field {fieldName} was not found on {typeof(TTarget)}");
            }

            return (TTarget t) => (TField)field.GetValue(t);
        }

        public static Delegate CreateInstanceDelegate(object source, string name)
        {
            var method = source.GetType().GetMethod(name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            if (method == null)
            {
                throw new ArgumentException($"Method '{name}' not found on {source.GetType()}");
            }

            Type[] types;
            if (method.ReturnType == typeof(void))
            {
                types = ActionDelegateTypes;
            }
            else
            {
                types = FuncDelegateTypes;
            }

            var parameters = method.GetParameters();
            if (parameters.Length >= types.Length)
            {
                throw new ArgumentException($"Too many parameters for delegate types:{parameters.Length}");
            }

            Type delegateType;
            var openDelegateType = types[parameters.Length];
            if (openDelegateType.IsGenericType)
            {
                var genericArgTypes = new List<Type>(parameters.Select(p => p.ParameterType));

                
                if (method.ReturnType == typeof(void))
                {
                    delegateType = openDelegateType.MakeGenericType(genericArgTypes.ToArray());
                }
                else
                {
                    genericArgTypes.Add(method.ReturnType);
                    delegateType = openDelegateType.MakeGenericType(genericArgTypes.ToArray());
                }
            }
            else
            {
                delegateType = openDelegateType;
            }

            var del = Delegate.CreateDelegate(delegateType, source, method);
            return del;
        }
    }
}

