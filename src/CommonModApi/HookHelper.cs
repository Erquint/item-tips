﻿using System;
using System.Reflection;
using MonoMod.RuntimeDetour;

namespace CommonModApi
{
    internal static class HookHelper
    {
        public static Hook CreateHook(Type sourceType, string sourceMethodName, object newReceiver, string hookMethodName)
        {
            var sourceMethod = sourceType.GetMethod(sourceMethodName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            var hookDelegate = ReflectionHelper.CreateInstanceDelegate(newReceiver, hookMethodName);
            Hook hook = new Hook(sourceMethod, hookDelegate);
            return hook;
        }
    }
}

