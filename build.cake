#addin nuget:?package=Cake.Json&version=7.0.1
#addin nuget:?package=Newtonsoft.Json&version=11.0.2

#tool nuget:?package=NUnit.ConsoleRunner&version=3.15.2

//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////

var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");

//////////////////////////////////////////////////////////////////////
// PREPARATION
//////////////////////////////////////////////////////////////////////

var solutionFile = "./ItemTips.sln";

//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////

Task("Restore-NuGet-Packages")
    .Does(() =>
{
    NuGetRestore(solutionFile);
});

Task("Build")
    .IsDependentOn("Restore-NuGet-Packages")
    .Does(() =>
{
    var settings = new DotNetBuildSettings
    {
        Configuration = configuration
    };

    DotNetBuild(solutionFile, settings);
});

Task("Run-Unit-Tests")
    .IsDependentOn("Build")
    .Does(() =>
{
    NUnit3("./test/**/bin/" + configuration + "/*Tests.dll", new NUnit3Settings {
        NoResults = true
        });
});

Task("Clean-Publish")
    .Does(() => 
{
    CleanDirectories("./publish/**");
});

Task("Create-Zip-Files-ItemTips")
    .IsDependentOn("Run-Unit-Tests")
    .IsDependentOn("Clean-Publish")
    .Does(() => 
{
    var innerPublishDir = Directory("./publish/itemtips");
    CreateDirectory(innerPublishDir);

    CopyFiles("./metadata/itemtips/*", innerPublishDir);

    var outDir = Directory($"./src/ItemTipsMod/bin/{configuration}/net35");
    CopyFileToDirectory(outDir + File("ItemTipsMod.dll"), innerPublishDir);
    // pdbs don't seem to work
    // CopyFileToDirectory(outDir + File("ItemTipsMod.pdb"), innerPublishDir);

    var manifest = File("./metadata/itemtips/manifest.json");

    var manifestJson = ParseJsonFromFile(manifest);
    string version = manifestJson["version_number"].ToString();

    var zipFile = File($"./publish/ItemTipsMod_{version}.zip");
    Information($"Creating zip for version:{version}, Zip:{zipFile}");

    Zip(innerPublishDir, zipFile);
});

Task("Create-Zip-Files-TipPack")
    .IsDependentOn("Clean-Publish")
    .Does(() => 
{
    var innerPublishDir = Directory("./publish/tippack");
    CreateDirectory(innerPublishDir);

    CopyFiles("./data/*.tip", innerPublishDir);
    CopyFiles("./metadata/tippack/*", innerPublishDir);
    var manifest = File("./metadata/tippack/manifest.json");

    var manifestJson = ParseJsonFromFile(manifest);
    string version = manifestJson["version_number"].ToString();

    var zipFile = File($"./publish/TipPackMod_{version}.zip");
    Information($"Creating zip for version:{version}, Zip:{zipFile}");

    Zip(innerPublishDir, zipFile);
});

Task("Package")
    .IsDependentOn("Create-Zip-Files-ItemTips")
    .IsDependentOn("Create-Zip-Files-TipPack");

//////////////////////////////////////////////////////////////////////
// TASK TARGETS
//////////////////////////////////////////////////////////////////////

Task("Default")
    .IsDependentOn("Build");

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(target);