﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ItemTipsMod;
using NUnit.Framework;
using NUnit.Framework.Interfaces;

[assembly: ModTests.TestLogging]

namespace ModTests
{
    [AttributeUsage(AttributeTargets.Assembly)]
    internal sealed class TestLoggingAttribute : TestActionAttribute
    {
        public override ActionTargets Targets => ActionTargets.Suite;

        public override void BeforeTest(ITest test)
        {
        }
    }
}
